package iterator

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

internal class BookShelfTest {
    @Test
    fun getBookAt() {
        val bookShelf = BookShelf(5)
        bookShelf.appendBook(Book("my-book1"))
        bookShelf.appendBook(Book("my-book2"))
        assertEquals("my-book2", bookShelf.getBookAt(1).name)
    }

    @Test
    fun appendBook() {
        val bookShelf = BookShelf(5)
        bookShelf.appendBook(Book("my-book"))
        assertEquals("my-book", bookShelf.getBookAt(0).name)
    }

    @Test
    fun getLength() {
        val bookShelf = BookShelf(5)
        bookShelf.appendBook(Book("my-book"))
        assertEquals(1, bookShelf.getLength())
    }
}
