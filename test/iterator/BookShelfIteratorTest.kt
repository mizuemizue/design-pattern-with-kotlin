package iterator

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class BookShelfIteratorTest {
    @Test
    fun hasNext() {
        val bookShelf = BookShelf(5)
        val itr = bookShelf.iterator()
        assertEquals(itr.hasNext(), false)
        bookShelf.appendBook(Book("my-book"))
        val itr2 = bookShelf.iterator()
        assertEquals(itr2.hasNext(), true)
    }

    @Test
    operator fun next() {
        val bookShelf = BookShelf(5)
        bookShelf.appendBook(Book("my-book"))
        val itr = bookShelf.iterator()
        assertEquals(itr.next().name, "my-book")
    }
}
