package builder

import kotlin.system.exitProcess

fun main(args: Array<String>) {
    if (args.isEmpty()) {
        usage()
        exitProcess(0)
    }

    val builder: Builder = when(args[0]) {
        "plain" -> {
            TextBuilder()
        }
        "html" -> {
            HTMLBuilder()
        }
        else -> {
            usage()
            exitProcess(0)
        }
    }
    val director = Director(builder)
    director.construct()
    val result = builder.getResult()
    println(result)
}

fun usage() {
    println("plain でプレーンテキスト文書")
    println("html でHTML文書")
}
