package builder

import java.io.FileWriter
import java.io.PrintWriter

class HTMLBuilder(): Builder {
    private var filename: String = ""
    private lateinit var writer: PrintWriter
    override fun makeTitle(title: String) {
        filename = "$title.html"
        writer = PrintWriter(FileWriter(filename))
        writer.println("<html><head><title>$title</title></head><body>")
        writer.println("<h1>$title</h1>")
    }

    override fun makeString(string: String) {
        writer.println("<p>$string</p>")
    }

    override fun makeItems(items: Array<String>) {
        writer.println("<ul>")
        for (item in items) {
            writer.println("<li>$item</li>")
        }
        writer.append("</ul>")
    }

    override fun close() {
        writer.println("</body></html>")
        writer.close()
    }
    override fun getResult(): String {
        return filename
    }
}
