package builder

class Director(val builder: Builder) {
    fun construct() {
        builder.makeTitle("Greeting")
        builder.makeString("朝から昼にかけて")
        builder.makeItems(arrayOf("こんにちは", "おはようございます"))
        builder.makeString("夜に")
        builder.makeItems(arrayOf("こんばんは", "おやすみなさい", "さようなら"))
        builder.close()
    }
}
