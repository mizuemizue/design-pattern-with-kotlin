package builder

class TextBuilder: Builder {
    private val buffer: StringBuilder = StringBuilder()
    override fun makeTitle(title: String) {
        buffer.append("=================================\n")
        buffer.append("[$title]\n")
        buffer.append("\n")
    }

    override fun makeString(string: String) {
        buffer.append("$string\n")
        buffer.append("\n")
    }

    override fun makeItems(items: Array<String>) {
       for (item in items) {
           buffer.append(" ・$item\n")
       }
        buffer.append("\n")
    }

    override fun close() {
        buffer.append("=================================\n")
    }
    override fun getResult(): String {
        return buffer.toString()
    }
}
