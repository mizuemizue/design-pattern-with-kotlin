package builder

interface Builder {
    fun makeTitle(title: String)
    fun makeString(string: String)
    fun makeItems(items: Array<String>)
    fun close()
    fun getResult(): String
}
