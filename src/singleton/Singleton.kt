package singleton

class Singleton private constructor() {
    var callCount = 0
        private set // setterのみprivateにできる
    init {
        println("Singleton class instance initialized!!!")
    }
    companion object {
        private val instance = Singleton()
        fun getInstance(): Singleton {
            instance.callCount++
            return instance
        }
    }
}
