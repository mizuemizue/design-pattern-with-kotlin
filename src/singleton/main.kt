package singleton

fun main() {
    // Singleton() Constructor is private
    val in1 = Singleton.getInstance() // callCount++ 1
    val in2 = Singleton.getInstance() // callCount++ 2

    println(in1.callCount)
    println(in2.callCount)

    val in3 = Singleton.getInstance() // callCount++ 3
    println(in3.callCount)
}
