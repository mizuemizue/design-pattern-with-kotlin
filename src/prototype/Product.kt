package prototype

interface Product {
    fun use(string: String)
    fun clone(): Product
}
