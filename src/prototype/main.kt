package prototype

fun main() {
    val manager = Manager()

    val mBox1 = MessageBox('/')
    val mBox2 = MessageBox('?')
    val mBox3 = MessageBox('*')
    val uPen1 = UnderlinePen('_')

    manager.register("m1", mBox1)
    manager.register("m2", mBox2)
    manager.register("m3", mBox3)
    manager.register("up1", uPen1)

    val mBox1Dash = manager.create("m1")
    val mBox2Dash = manager.create("m2")
    val mBox3Dash = manager.create("m3")
    val uPen1Dash = manager.create("up1")

    mBox1.use("Hello")
    mBox2.use("Hello")
    mBox3.use("Hello")
    uPen1.use("Hello")
    mBox1Dash?.use("Hello")
    mBox2Dash?.use("Hello")
    mBox3Dash?.use("Hello")
    uPen1Dash?.use("Hello")
}
