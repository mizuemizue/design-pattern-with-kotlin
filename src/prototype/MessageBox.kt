package prototype

class MessageBox(decoChar: Char): Product {
    var decoChar = decoChar
        private set

    constructor(messageBox: MessageBox): this(messageBox.decoChar)

    override fun use(string: String) {
        val length = string.length
        printLine(length)
        println("$decoChar $string $decoChar")
        printLine(length)
    }

    override fun clone(): Product {
        return MessageBox(this)
    }

    private fun printLine(length: Int) {
        for (i in 0..length) print(decoChar)
        println()
    }
}
