package prototype

class UnderlinePen(ulChar: Char) : Product {
    var ulChar: Char = ulChar
        private set

    constructor(underlinePen: UnderlinePen): this(underlinePen.ulChar)

    override fun use(string: String) {
        val length = string.length
        println("\" $string \"")
        for (i in 0..length) print(ulChar)
        println()
    }

    override fun clone(): Product {
        return UnderlinePen(this)
    }
}
