package prototype

class Manager {
    private val showcase: HashMap<String, Product> = HashMap()
    fun register(name: String, product: Product) {
        showcase.put(name, product)
    }

    fun create(name: String): Product? {
       val target = showcase.get(name)
        if (target != null) {
            return target.clone()
        }
        return null
    }
}
