package adapter

open class Banner(private val string: String) {
//    protected fun showWithParen() {
    fun showWithParen() {
        println("($string)")
    }

//    protected fun showWithAater() {
    fun showWithAater() {
        println("*$string*")
    }
}
