package adapter

fun main() {
    val banner: Print
    banner = PrintBanner("My name is mizushima.")
    banner.printStrong()
    banner.printWeak()
}
