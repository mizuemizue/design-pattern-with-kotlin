package adapter

//class PrintBanner(string: String) : Print, Banner(string) {
class PrintBanner(string: String) : Print {
    private val banner: Banner = Banner(string)
    override fun printWeak() {
//        super.showWithParen()
        banner.showWithParen()
    }

    override fun printStrong() {
//        super.showWithAater()
        banner.showWithAater()
    }
}
