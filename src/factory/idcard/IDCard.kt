package factory.idcard

import factory.framework.Product

class IDCard(private val owner: String): Product() {
    override fun use() {
        println("${owner}のIDCardを使用します。")
    }

    fun getOwner(): String {
        return owner
    }
}
