package factory.idcard

import factory.framework.Factory
import factory.framework.Product

class IDCardFactory: Factory() {
    private val list = arrayListOf<Product>()

    override fun createProduct(owner: String): Product {
        println("${owner}のIDCardを作成します。")
        return IDCard(owner)}

    override fun registerProduct(product: Product) {
        list.add(product)
    }
}
