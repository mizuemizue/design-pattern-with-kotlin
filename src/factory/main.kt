package factory

import factory.idcard.IDCardFactory

fun main() {
    val factory = IDCardFactory()
    val p1 = factory.create("mizushima")
    val p2 = factory.create("tanaka")
    val p3 = factory.create("suzuki")
    p1.use()
    p2.use()
    p3.use()
}
