package template

fun main() {
    val cd = CharDisplay('A')
    cd.display()

    val cd2 = StringDisplay("Hello, World!!!")
    cd2.display()

    val cd3 = StringDisplay("こんにちは。")
    cd3.display()
}
