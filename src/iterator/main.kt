package iterator

fun main() {
    val bookshelf = BookShelf(4)
    bookshelf.appendBook(Book("デザパタ"))
    bookshelf.appendBook(Book("投資"))
    bookshelf.appendBook(Book("DDD"))
    bookshelf.appendBook(Book("クリーンアーキテクチャ"))

    val itr = bookshelf.iterator()

    while (itr.hasNext()) {
        val book = itr.next()
        println(book.name)
    }
}
