package iterator

class BookShelf(maxsize: Int): Aggregate<Book> {
    private var last: Int = 0
    // TODO nullでFillした方がメモリ効率はよさそうだけど、Book?になってしまうのがどうなのかと思わなくもない
    // private var books: Array<Book?> = arrayOfNulls(maxsize)
    private var books: Array<Book> = Array(maxsize) { Book("") }

    override fun iterator(): Iterator<Book> {
        return BookShelfIterator(this)
    }

    fun getBookAt(index: Int): Book {
        return books[index]
    }

    fun appendBook(book: Book) {
        books[last] = book
        last++
    }

    fun getLength(): Int {
        return last
    }
}
